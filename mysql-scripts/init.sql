DROP TABLE IF EXISTS ETAPE_HISTORY;
DROP TABLE IF EXISTS ROUTINE_HISTORY; 
DROP TABLE IF EXISTS ETAPE;
DROP TABLE IF EXISTS ROUTINE;
DROP TABLE IF EXISTS THEME;
DROP TABLE IF EXISTS UTILISATEUR;
DROP TABLE IF EXISTS TABLETTE;

CREATE TABLE TABLETTE(
   id_tablette VARCHAR(36),
   PRIMARY KEY(id_tablette)
);

CREATE TABLE UTILISATEUR(
   id_utilisateur SMALLINT AUTO_INCREMENT ,
   nom VARCHAR(50)  NOT NULL,
   path_image VARCHAR(200) NOT NULL,
   couleur VARCHAR(50)  NOT NULL,
   rang_liste TINYINT NOT NULL,
   id_tablette VARCHAR(36)  NOT NULL,
   PRIMARY KEY(id_utilisateur),
   UNIQUE(nom, id_tablette),
   UNIQUE(rang_liste, id_tablette),
   FOREIGN KEY(id_tablette) REFERENCES TABLETTE(id_tablette) ON DELETE CASCADE
);

CREATE TABLE THEME(
   id_theme SMALLINT AUTO_INCREMENT ,
   nom VARCHAR(50)  NOT NULL,
   path_image VARCHAR(200) NOT NULL,
   couleur VARCHAR(50)  NOT NULL,
   rang_liste TINYINT NOT NULL,
   id_utilisateur SMALLINT NOT NULL,
   PRIMARY KEY(id_theme),
   UNIQUE(nom, id_utilisateur),
   UNIQUE(rang_liste, id_utilisateur),
   FOREIGN KEY(id_utilisateur) REFERENCES UTILISATEUR(id_utilisateur) ON DELETE CASCADE
);

CREATE TABLE ROUTINE(
   id_routine SMALLINT AUTO_INCREMENT ,
   nom VARCHAR(50)  NOT NULL,
   path_image VARCHAR(200) NOT NULL,
   couleur VARCHAR(50)  NOT NULL,
   rang_liste TINYINT NOT NULL,
   id_theme SMALLINT NOT NULL,
   PRIMARY KEY(id_routine),
   UNIQUE(nom, id_theme),
   UNIQUE(rang_liste, id_theme),
   FOREIGN KEY(id_theme) REFERENCES THEME(id_theme) ON DELETE CASCADE
);

CREATE TABLE ROUTINE_HISTORY(
   routine_history SMALLINT AUTO_INCREMENT,
   id_routine SMALLINT NOT NULL,
   PRIMARY KEY(routine_history),
   FOREIGN KEY(id_routine) REFERENCES ROUTINE(id_routine)
);

CREATE TABLE ETAPE(
   id_etape SMALLINT AUTO_INCREMENT ,
   consigne VARCHAR(50)  NOT NULL,
   path_image VARCHAR(200) NOT NULL,
   rang_liste TINYINT NOT NULL,
   audio_path VARCHAR(50),
   est_lue BOOLEAN NOT NULL,
   id_routine SMALLINT NOT NULL,
   PRIMARY KEY(id_etape),
   UNIQUE(rang_liste, id_routine),
   FOREIGN KEY(id_routine) REFERENCES ROUTINE(id_routine) ON DELETE CASCADE
);

CREATE TABLE ETAPE_HISTORY(
   routine_history SMALLINT,
   id_etape SMALLINT,
   date_debut DATETIME NOT NULL,
   date_fin DATETIME NOT NULL,
   aide BOOLEAN NOT NULL,
   retour BOOLEAN NOT NULL,
   PRIMARY KEY(routine_history, id_etape),
   FOREIGN KEY(routine_history) REFERENCES ROUTINE_HISTORY(routine_history),
   FOREIGN KEY(id_etape) REFERENCES ETAPE(id_etape) ON DELETE CASCADE
);

ALTER TABLE ETAPE_HISTORY ADD CHECK (date_fin > date_debut);

