<?php

    include_once("config.php");
    $linesContent = "";
    
    try {
		
		$query_result = $pdo->query("SELECT id_tablette, (SELECT MIN(date_debut) FROM ETAPE_HISTORY
JOIN ROUTINE_HISTORY ON ETAPE_HISTORY.routine_history = ROUTINE_HISTORY.routine_history WHERE ROUTINE_HISTORY.routine_history = r.routine_history) AS date_debut,
ROUTINE.nom AS nom_routine,
UTILISATEUR.nom AS nom_utilisateur,
TIMEDIFF((
    SELECT MAX(date_fin) FROM ETAPE_HISTORY
JOIN ROUTINE_HISTORY ON ETAPE_HISTORY.routine_history = ROUTINE_HISTORY.routine_history WHERE ROUTINE_HISTORY.routine_history = r.routine_history),
    (SELECT MIN(date_debut) FROM ETAPE_HISTORY
JOIN ROUTINE_HISTORY ON ETAPE_HISTORY.routine_history = ROUTINE_HISTORY.routine_history WHERE ROUTINE_HISTORY.routine_history = r.routine_history)) AS difference_temps,
r.routine_history
FROM ROUTINE_HISTORY AS r JOIN ROUTINE ON r.id_routine = ROUTINE.id_routine JOIN THEME ON ROUTINE.id_theme = THEME.id_theme JOIN UTILISATEUR ON THEME.id_utilisateur = UTILISATEUR.id_utilisateur WHERE (SELECT MIN(date_debut) FROM ETAPE_HISTORY
JOIN ROUTINE_HISTORY ON ETAPE_HISTORY.routine_history = ROUTINE_HISTORY.routine_history WHERE ROUTINE_HISTORY.routine_history = r.routine_history) IS NOT NULL ORDER BY date_debut ASC;
");

        if($query_result != false)
        {
            $linesContent = $query_result->fetchAll();
        }

    } catch(PDOException $e) {
        $e->getMessage();
    }

    echo json_encode($linesContent);
?>