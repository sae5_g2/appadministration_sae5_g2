<?php

    include_once("config.php");
    
    try {
		
		$query_result = $pdo->query("SELECT id_tablette, UTILISATEUR.nom AS nom_utilisateur, (SELECT MIN(date_debut) FROM ETAPE_HISTORY
JOIN ROUTINE_HISTORY ON ETAPE_HISTORY.routine_history = ROUTINE_HISTORY.routine_history WHERE ROUTINE_HISTORY.routine_history = r.routine_history) AS date_debut,
ROUTINE.nom AS nom_routine,
TIMEDIFF((
    SELECT MAX(date_fin) FROM ETAPE_HISTORY
JOIN ROUTINE_HISTORY ON ETAPE_HISTORY.routine_history = ROUTINE_HISTORY.routine_history WHERE ROUTINE_HISTORY.routine_history = r.routine_history),
    (SELECT MIN(date_debut) FROM ETAPE_HISTORY
JOIN ROUTINE_HISTORY ON ETAPE_HISTORY.routine_history = ROUTINE_HISTORY.routine_history WHERE ROUTINE_HISTORY.routine_history = r.routine_history)) AS difference_temps
FROM ROUTINE_HISTORY AS r JOIN ROUTINE ON r.id_routine = ROUTINE.id_routine JOIN THEME ON ROUTINE.id_theme = THEME.id_theme JOIN UTILISATEUR ON THEME.id_utilisateur = UTILISATEUR.id_utilisateur WHERE (SELECT MIN(date_debut) FROM ETAPE_HISTORY
JOIN ROUTINE_HISTORY ON ETAPE_HISTORY.routine_history = ROUTINE_HISTORY.routine_history WHERE ROUTINE_HISTORY.routine_history = r.routine_history) IS NOT NULL ORDER BY date_debut ASC;
");

        if($query_result != false)
        {
        	$output = fopen("php://output", "w");

            $delimiter = ";";

            $results = $query_result->fetchAll(PDO::FETCH_ASSOC);

            //Ajout du nom des colonnes de la table à l'en tête du CSV
          	$columns = array();
          	foreach($results[0] as $key => $value)
          	{
          		array_push($columns, $key);
          	}
          	fputcsv($output, $columns, $delimiter);

          	//Ajout du corps du fichier CSV
            foreach($results as $result)
            {
                $line = array();
                foreach($result as $key => $value)
                {
                    array_push($line, $value);
                }
                fputcsv($output, $line, $delimiter);
            }
        }

        header('Content-Type: text/csv'); 
    	header('Content-Disposition: attachment; filename="routine.csv";');
    	fclose($output);

    } catch(PDOException $e) {
        $e->getMessage();
    }

    exit();
?>