<?php

    include_once("config.php");
    
    try {
		
		$query_result = $pdo->query("SELECT rang_liste, consigne, TIMEDIFF(date_fin, date_debut) AS temps, aide FROM ETAPE JOIN ETAPE_HISTORY ON ETAPE.id_etape = ETAPE_HISTORY.id_etape WHERE routine_history =" . $_GET["routine_history"] . ";");

        if($query_result != false)
        {
        	$output = fopen("php://output", "w");

            $delimiter = ";";

            $results = $query_result->fetchAll(PDO::FETCH_ASSOC);

            //Ajout du nom des colonnes de la table à l'en tête du CSV
          	$columns = array();
          	foreach($results[0] as $key => $value)
          	{
          		array_push($columns, $key);
          	}
          	fputcsv($output, $columns, $delimiter);

          	//Ajout du corps du fichier CSV
            foreach($results as $result)
            {
                $line = array();
                foreach($result as $key => $value)
                {
                    array_push($line, $value);
                }
                fputcsv($output, $line, $delimiter);
            }
        }

        header('Content-Type: text/csv'); 
    	header('Content-Disposition: attachment; filename="steps.csv";');
    	fclose($output);

    } catch(PDOException $e) {
        $e->getMessage();
    }

    exit();
?>