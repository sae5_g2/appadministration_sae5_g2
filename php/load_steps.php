<?php
    include_once("config.php");
	
    $linesContent = array();
    
    try {
		
		$query_result = $pdo->query("SELECT rang_liste, consigne, TIMEDIFF(date_fin, date_debut) AS temps, aide FROM ETAPE JOIN ETAPE_HISTORY ON ETAPE.id_etape = ETAPE_HISTORY.id_etape WHERE routine_history =" . $_GET["routine_history"] . ";");

        if($query_result != false)
        {
            $linesContent = $query_result->fetchAll();
        }

    } catch(PDOException $e) {
        $e->getMessage();
    }

    echo json_encode($linesContent);
?>