# Site web d'administration et de visualisation des données de Routine'App

## Introduction

Ce site web est actuellement hébergé sur le serveur LAMP 172.30.13.104 accessible uniquement sur le réseau de l'université à l'heure actuelle (02/2024). Il permet au client de visualiser les données liées aux routines et aux étapes accomplies par les utilisateurs et de les analyser pour repérer par exemple les utilisateurs en difficultés.

## Arborescence

- *mysql-scripts* : Tous les script SQL de création des tables, les fichiers contenant des données de tests (.csv) à importer dans les tables correspondantes et la documentation de la base de données.

- *ressources* : Tous les fichiers n'étant pas du code sources utilisables dans le site web (images, vidéos, ...)

- *php* : Tous les fichiers .php (à part index.php qui sert à vérifier le bon fonctionnement de php)

- *static* : Contient le fichier min permettant d'utiliser la bibliothèque jQuery
